# Customisations:

## Récupérer le Teacher courant lors de la création d'un nouveau TERSubject:
Dans [SubjectController](src/main/java/um/fds/agl/ter22/controllers/SubjectController.java),
lors d'une requête POST pour créér un nouveau sujet, on récupérera l'username du teacher authentifié :

```java
String username;
        Object principal = SecurityContextHolder. getContext(). getAuthentication(). getPrincipal();
        if (principal instanceof UserDetails) {

             username = ((UserDetails)principal). getUsername();
        } else {
             username = principal. toString();
        }
        Teacher teacher = teacherRepository.findByLastName(username);

```