package um.fds.agl.ter22.entities;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
public class TERSubject {

    // @ManyToOne car many subjects can be related to one teacher
        private @ManyToOne Teacher teacher;
        private @ManyToMany Set<Teacher> secondaryTeachers;

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Set<Teacher> getSecondaryTeachers(){
        return secondaryTeachers;
    }
    public void setSecondaryTeachers(Set<Teacher> secondaryTeachers) {
        this.secondaryTeachers = secondaryTeachers;
    }

    private String title;

    public String getTitle(){
        return title;
    }
    public void setTitle(String title){
        this.title = title;
    }


    public TERSubject(){}

    public TERSubject(Teacher teacher, String title ){
        this.teacher = teacher;
        this.title = title;
    }

    private @Id @GeneratedValue Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }


    @Override
    public String toString() {
        return "TERSubject{" +
                "id='" + getId() + "'"+
                ", title='" + getTitle() +"'"+
                ", teacher='" + getTeacher() + "'"+
                "}";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TERSubject that = (TERSubject) o;
        return Objects.equals(teacher, that.teacher) && Objects.equals(title, that.title) && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teacher, title, id);
    }


}
