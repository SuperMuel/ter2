package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.TERSubject;
import um.fds.agl.ter22.repositories.TERSubjectRepository;

import java.util.Optional;

@Service
public class TERSubjectService {

    @Autowired
    private TERSubjectRepository subjectRepository;


    public Optional<TERSubject> getSubject(final Long id){
        return subjectRepository.findById(id);
    }

    public Iterable<TERSubject> getSubjects(){
        return subjectRepository.findAll();
    }


    public void deleteSubject(final Long id){
        subjectRepository.deleteById(id);
    }

    public TERSubject saveSubject(TERSubject subject){
        return subjectRepository.save(subject);
    }


    public Optional<TERSubject> findById(Long id){
        return subjectRepository.findById(id);
    }




}
