package um.fds.agl.ter22.repositories;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.TERSubject;

import java.util.Optional;

public interface TERSubjectRepository extends CrudRepository<TERSubject, Long> {


    //@PreAuthorize("hasRole('ROLE_TEACHER')")
    TERSubject save(@Param("subject") TERSubject terSubject);


    @Override
    Optional<TERSubject> findById(Long id);

    // Subjects are public
    Iterable<TERSubject> findAll();

    // Optional<T> findByTitle(String title);

    //TODO: find why this line causes server to not start
    // @PreAuthorize("@TERSubjectRepository.findById(#id).get()?.teacher?.lastName == authentication?.name")
    void deleteById(@Param("id") Long id);

    @PreAuthorize("#terSubject?.teacher?.lastName == authentication?.name")
    void delete(@Param("terSubject") TERSubject terSubject);

}

