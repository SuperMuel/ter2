package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.TERManager;
import um.fds.agl.ter22.entities.TERSubject;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.forms.TERSubjectForm;
import um.fds.agl.ter22.repositories.TeacherRepository;
import um.fds.agl.ter22.services.SpringDataJpaUserDetailsService;
import um.fds.agl.ter22.services.TERSubjectService;

import java.util.Optional;

@Controller
public class SubjectController {
    @Autowired
    private TERSubjectService terSubjectService;

    @Autowired
    private TeacherRepository teacherRepository;

    @GetMapping(value = "/listTERSubjects")
    public Iterable<TERSubject> getTERSubjects(Model model){
        Iterable<TERSubject> terSubjects = terSubjectService.getSubjects();
        model.addAttribute("terSubjects", terSubjects);
        return terSubjects;
    }

    // TODO add : @PreAuthorize("hasRole('ROLE_TEACHER')")
    @GetMapping(value="/addTERSubject")
    public String showAddTERSubjectPage(Model model){
        TERSubjectForm form = new TERSubjectForm();
        model.addAttribute("terSubjectForm", form);
        return "addTERSubject";
    }
//TODO showTERSubjectUpdateForm/

    @PostMapping(value = "/addTERSubject")
    public String addTERSubject(Model model, @ModelAttribute("TERSubjectForm") TERSubjectForm terSubjectForm){


        String username;
        Object principal = SecurityContextHolder. getContext(). getAuthentication(). getPrincipal();
        if (principal instanceof UserDetails) {

             username = ((UserDetails)principal). getUsername();
        } else {
             username = principal. toString();
        }
        Teacher teacher = teacherRepository.findByLastName(username);

        TERSubject s;

        s = new TERSubject(teacher, terSubjectForm.getTitle());

        terSubjectService.saveSubject(s);
        return "redirect:/listTERSubjects";
    }



    @GetMapping(value="/deleteTERSubject/{id}")
    public String deleteTERSubject(Model model, @PathVariable("id") Long id){
        //TODO Check for rights?

        terSubjectService.deleteSubject(id);
        return "redirect:/listTERSubjects";
    }


}
