package um.fds.agl.ter22.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.security.test.context.support.WithMockUser;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.repositories.TeacherRepository;
import um.fds.agl.ter22.services.TeacherService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean // injectera automatiquement le mock
    private TeacherService teacherService;

    @Autowired
    private TeacherRepository teacherRepository;

    // @Test
    // @WithMockUser(username = "Chef", roles = "MANAGER")
    // void addTeacherGet() throws Exception {

    //     mvc.perform(get("/addTeacher"))
    //             .andExpect(status().isOk())
    //             .andExpect(content().contentType("text/html;charset=UTF8"))
    //             .andExpect(view().name("addTeacher"))
    //             .andReturn();
    // }

    // @Test
    // @WithMockUser(username = "Chef", roles = "MANAGER")
    // void addTeacherPostNonExistingTeacher() throws Exception {
    //     assertTrue(teacherService.getTeacher(10L).isEmpty());
    //     MvcResult result = mvc.perform(post("/addTeacher")
    //                     .param("firstName", "Anne-Marie")
    //                     .param("lastName", "Kermarrec")
    //                     .param("id", "10")
    //             )
    //             .andExpect(status().is3xxRedirection())
    //             .andReturn();

    //     ArgumentCaptor<Teacher> argumentCaptor = ArgumentCaptor.forClass(Teacher.class);

    //     verify(teacherService).saveTeacher(argumentCaptor.capture()); // vérifie que save a été appelé

    //     Teacher teacher = argumentCaptor.getValue();
    //     assertEquals(teacher.getId(), 10L);
    //     assertEquals(teacher.getFirstName(), "Anne-Marie");
    //     assertEquals(teacher.getLastName(), "Kermarrec");
    // }
}