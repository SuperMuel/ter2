package seleniumTests;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddTeacherTest extends BaseForTests {
    @Test
    void managerCanAddTeacher() throws IOException {
        login("Chef", "mdp");
        get("/addTeacher");
        WebElement firstNameField = driver.findElement(By.id("firstName"));
        WebElement lastNameField = driver.findElement(By.id("lastName"));
        WebElement loginButton = driver.findElement(By.cssSelector("[type=submit]"));

        write(firstNameField, "TeacherFirstNameForTest");
        write(lastNameField, "TeacherLastNameForTest");
        click(loginButton);

        WebElement teacherListPageTitle = driver.findElement(By.tagName("h1"));
        waitElement(teacherListPageTitle);

        assertTrue(teacherListPageTitle.getText().contains("Teacher List"));

        assertTrue(driver.getPageSource().contains("TeacherFirstNameForTest"));

        screenshot("teacher-was-added-by-manager");

    }
}
